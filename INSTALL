INSTALLATION INSTRUCTIONS:

   $ ./configure
   $ make
   # make install

This will not install the documentation, which is shipped as
pre-prepared HTML and PostScript as well as debiandoc-sgml source.
Put that (spec.html/ and spec.ps) where you will.

SYSTEM REQUIREMENTS:

Programs:

* md5sum (GNU textutils; alternatively, Colin Plumb's, as shipped with
          Debian in the `dpkg' package) - used during build only
* GNU make
* GCC is preferred but other compilers ought to work (though
  no portability testing has yet been done).  ANSI C only.
  ints must be at least 32 bits.
* A sensible `cat' which notices write errors (ie not SunOS4, BSD 4.3,
  or many others.  GNU textutils has one.  NB that this `cat' is used
  at runtime and must be first on the system default PATH, so probably
  in /bin.)
* GNU m4   } if you don't want to change those bits of source
* GNU flex }  you can do without these

C Library:

* [v]snprintf - a real version, not just [v]sprintf with a wrapper
  that throws the argument away.
* strsignal;
* fnmatch;
* BSD syslog(3);
* strtoul;
* memcpy, memset, memcpy;
* realloc(0,size) must work and be equivalent to malloc(size).
* free(0) must work and do nothing
* <stdarg.h> (not varargs) and v[sf][n]printf.

System interfaces:

* setreuid(2), getreuid(2), getgroups(2), initgroups(3), with
  the ability for root to (a) swap euid and ruid and
  (b) give away all privilege by calling setreuid(ruid,ruid)
  twice.
* wait3 and waitpid, <wait.h> with WNOHANG, WIFSIGNALED,
  WIFEXITEED, WTERMSIG, WEXITSTATUS and WCOREDUMP.
* gid_t, uid_t, pid_t.
* Unix-domain (AF_UNIX) stream sockets, for use with:
  * BSD sockets - socket(), bind(), listen(), accept(), connect();
  * socketpair(2);
* lstat(2) (though stat(2) will be safe on systems without symlinks,
  if you say -Dlstat=stat).
* Pipes:
  * creating using pipe(2) and mkfifo(2);
  * proper interaction between open(O_RDWR), open(O_RDONLY),
    open(O_WRONLY), close(), dup2, EPIPE, SIGPIPE, &c.
    (ie, opening pipes with O_RDWR never blocks; EPIPE happens
    if you write with no readers; EOF happens if you read with
    no buffered data and writers);
* POSIX signal handling - sigaction(2), sigprocmask(2), sigsuspend(2);
* POSIX sessions - setsid(2) (for -daemon flag).

To format the documentation:

* debiandoc-sgml, and hence sp (aka nsgmls) and sgmlspm.
* For PostScript output, dblatex and XeTeX.

For debugging version (./configure --enable-debug):

* initgroups(3) must use setgroups(2) and dynamic
  linking must allow overriding setgroups(2) for initgroups(3);

EXIT STATUS CODES

For information about uservd's exit status, see uservd(8).

The daemon's per-request children will note the success level of its
request in its exit status.  This will not usually be logged unless it
is higher than those listed below; they are presented here for
completeness and as programming documentation.

 2 - The connection was just an internal version check.

 4 - The client requested that the service be disconnected.  This
     includes normal termination, which is achieved by having the
     server tell the client that the service has completed and waiting
     for the client to tell it to disconnect.

 8 - The client closed its end of the socket when this would not
     usually have been expected, causing an EPIPE or unexpected EOF in
     the server.  This is not an error condition - it can happen, for
     example, if the client receives a fatal signal of some kind from
     its execution environment (eg its controlling terminal).

 10 - The per-request child wishes the server to check whether it is
      still the uservd.

 12 - The service failed onm the service side in an expected and
      controlled manner, for example because it was rejected in the
      configuration files.

 16 - A fatal system call failure or other general error occurred,
      which ought not to have happened at all, barring system resource
      shortages.

 20 - The client sent invalid data to the server, after the client
      dropped all its system privilege.  On some systems this can be
      caused by a malicious calling user.

 SIGABRT/SIGIOT - The client sent invalid data to the server before it
   dropped all its system privileges, or some other unexpected
   internal error occurred.  This can also occur if an attempt to
   block signals using sigprocmask fails.

 0-3,5-7,9,11,13-15,17-19 are not currently used by normal children.

REENTRANCY IN THE LIBC:

We assume, both in the client and server, that it is safe to use one
stdio stream in a signal handler which interrupts use of a _different_
stdio stream in another.  We make sure using setvbuf that we have
pre-allocated buffers so that stdio doesn't need to use malloc() when
we actually read or write.  stdio had better not do anything else
weird.

Furthermore, we assume that it is safe to use syslog in a signal
handler which has interrupted a stdio operation (but we don't require
that it be safe to invoke when the signal has interrupted a call to
malloc, unless stdio makes gratuitous mallocs).  openlog will already
have been called (but syslog will not necessarily have been called).

We assume that strerror is completely reentrant.

PROBLEMS

* `function declaration isn't a prototype'

  One some systems (at least some versions of NetBSD, for example),
  the SIG_IGN and SIG_DFL macros contain function declarations (as part
  of a typecast, presumably) which are not prototypes.  The warning
  options that are used by default if the configure script detects that
  you're using a good GCC then cause the compilation to fail.  You must
  use
    make CFLAGS=-O2
  instead of just `make', thus suppressing warnings.

  The bug is actually in your system header files, for not specifying
  the number and types of arguments to signal handler functions when
  they cast in the SIG_IGN and SIG_DFL macros.

DEBUGGING VERSION

If you run configure with --enable-debug, a debugging version will be
built.  This will look in the current directory (the build directory)
for the base of various things, including the IPC area (which you must
therefore construct yourself).  The debugging version will produce
extra output at various points.  It will not attempt to call
setgroups(), instead just checking that the groups list is right, so
it will work non-setuid if the daemon is invoked as the service user.
The daemon won't fork for each request; instead, it will handle a
single request and exit.

There may be other changes.  Consult the code for details.  Making the
debugging version of the client or daemon setuid root is probably a
bad idea.  They may not work if they are run as different users.


COPYRIGHT

This file, INSTALL, contains installation instructions and other
details for userv.

userv is copyright Ian Jackson and other contributors.
See README for full authorship information.

userv is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation; either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
