/*
 * userv - acconfig.h
 * extra stuff for config.h.in (autoconf)
 *
 * userv is copyright Ian Jackson and other contributors.
 * See README for full authorship information.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with userv; if not, see <http://www.gnu.org/licenses/>.
 */

/* Define if EPROTO exists.  */
#undef HAVE_EPROTO

/* Define if LOG_AUTHPRIV exists.  */
#undef HAVE_LOG_AUTHPRIV

/* Define if WCOREDUMP exists.  */
#undef HAVE_WCOREDUMP

/* Define if function attributes a la GCC 2.5 and higher are available.  */
#undef HAVE_GNUC25_ATTRIB

/* Define if unused functions a la GCC 2.5 and higher are available.  */
#undef HAVE_GNUC25_UNUSED

/* Define if nonreturning functions a la GCC 2.5 and higher are available.  */
#undef HAVE_GNUC25_NORETURN

/* Define if printf-format argument lists a la GCC are available.  */
#undef HAVE_GNUC25_PRINTFFORMAT

@BOTTOM@

/* STRSIGNAL */
#ifndef HAVE_STRSIGNAL
#define STRSIGNAL(x) "[platform has no strsignal!]"
#endif

/* VSNPRINTF */
#ifndef HAVE_VSNPRINTF
# error "You must have vsnprintf!  Without vsnprintf it is very hard to write secure programs.  If you don't have it then your system libc is probably full of hideous buffer overrun security bugs.  But, if you don't want to fix your system a portable snprintf can be found at http://www.ijs.si/software/snprintf/"
#endif

/* EPROTO */
#ifndef HAVE_EPROTO
#define EPROTO 0
#endif

/* LOG_AUTHPRIV */
#ifndef HAVE_LOG_AUTHPRIV
#define LOG_AUTHPRIV LOG_AUTH
#endif

/* WCOREDUMP */
#ifndef HAVE_WCOREDUMP
#define WCOREDUMP(x) 0
#endif

/* GNU C attributes. */
#ifndef FUNCATTR
#ifdef HAVE_GNUC25_ATTRIB
#define FUNCATTR(x) __attribute__(x)
#else
#define FUNCATTR(x)
#endif
#endif

/* GNU C printf formats, or null. */
#ifndef ATTRPRINTF
#ifdef HAVE_GNUC25_PRINTFFORMAT
#define ATTRPRINTF(si,tc) format(printf,si,tc)
#else
#define ATTRPRINTF(si,tc)
#endif
#endif
#ifndef PRINTFFORMAT
#define PRINTFFORMAT(si,tc) FUNCATTR((ATTRPRINTF(si,tc)))
#endif

/* GNU C nonreturning functions, or null. */
#ifndef ATTRNORETURN
#ifdef HAVE_GNUC25_NORETURN
#define ATTRNORETURN noreturn
#else
#define ATTRNORETURN
#endif
#endif
#ifndef NONRETURNING
#define NONRETURNING FUNCATTR((ATTRNORETURN))
#endif

/* Combination of both the above. */
#ifndef NONRETURNPRINTFFORMAT
#define NONRETURNPRINTFFORMAT(si,tc) FUNCATTR((ATTRPRINTF(si,tc),ATTRNORETURN))
#endif

/* GNU C unused functions, or null. */
#ifndef ATTRUNUSED
#ifdef HAVE_GNUC25_UNUSED
#define ATTRUNUSED unused
#else
#define ATTRUNUSED
#endif
#endif
#ifndef UNUSED
#define UNUSED FUNCATTR((ATTRUNUSED))
#endif
