/*
 * userv - both.h
 * Useful very-low-level utility routines' declarations,
 * for both client and daemon.
 *
 * userv is copyright Ian Jackson and other contributors.
 * See README for full authorship information.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with userv; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOTH_H
#define BOTH_H

/* provided by both.c */

void *xmalloc(size_t s);
void *xrealloc(void *p, size_t s);
char *xstrsave(const char *s);

int working_getc(FILE *file);
size_t working_fread(void *ptr, size_t sz, FILE *file);

/* used by both.c, so must be present */

void syscallerror(const char *what) NONRETURNING;

#define ISCHAR(iswotsit,ch) (iswotsit((unsigned char)(ch))) /*Feh!*/

#endif
