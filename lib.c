/*
 * userv - lib.c
 * useful utility routines, used in daemon, but not very dependent on it
 *
 * userv is copyright Ian Jackson and other contributors.
 * See README for full authorship information.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with userv; if not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <syslog.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>

#include "config.h"
#include "common.h"
#include "lib.h"
#include "both.h"

char *xstrcat3save(const char *a, const char *b, const char *c) {
  char *r;

  r= xmalloc(strlen(a)+strlen(b)+strlen(c)+1);
  strcpy(r,a);
  strcat(r,b);
  strcat(r,c);
  return r;
}

char *xstrsubsave(const char *begin, int len) {
  char *r;
  
  r= xmalloc(len+1);
  memcpy(r,begin,len);
  r[len]= 0;
  return r;
}

int makeroom(char **buffer, int *size, int needed) {
  if (needed > MAX_GENERAL_STRING) return -1;
  if (*size >= needed) return 0;
  *buffer= xrealloc(*buffer,needed);
  *size= needed;
  return 0;
}

void vsnyprintf(char *buffer, size_t size, const char *fmt, va_list al) {
  vsnprintf(buffer,size,fmt,al);
  buffer[size-1]= 0;
}

void snyprintf(char *buffer, size_t size, const char *fmt, ...) {
  va_list al;
  va_start(al,fmt);
  vsnyprintf(buffer,size,fmt,al);
  va_end(al);
}

void strnycpy(char *dest, const char *src, size_t size) {
  strncpy(dest,src,size-1);
  dest[size-1]= 0;
}

void strnytcat(char *dest, const char *src, size_t size) {
  size_t l;
  l= strlen(dest);
  strnycpy(dest+l,src,size-l);
}

void vsnytprintfcat(char *buffer, size_t size, const char *fmt, va_list al) {
  size_t l;
  l= strlen(buffer);
  vsnyprintf(buffer+l,size-l,fmt,al);
}

void snytprintfcat(char *buffer, size_t size, const char *fmt, ...) {
  va_list al;
  va_start(al,fmt);
  vsnytprintfcat(buffer,size,fmt,al);
  va_end(al);
}

#ifndef HAVE_SETENV
int setenv(const char *name, const char *value, int overwrite) {
  char *buffer= 0;
  
  assert(overwrite==1);
  buffer= xmalloc(strlen(name)+strlen(value)+2);

  sprintf(buffer,"%s=%s",name,value);
  return putenv(buffer);
}
#endif /* HAVE_SETENV */
