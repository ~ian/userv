/*
 * userv - lib.c
 * useful utility routines' imports and exports, used in daemon
 *
 * userv is copyright Ian Jackson and other contributors.
 * See README for full authorship information.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with userv; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIB_H
#define LIB_H

char *xstrcat3save(const char *a, const char *b, const char *c);
char *xstrsubsave(const char *begin, int len);

void miscerror(const char *what) NONRETURNING;
int makeroom(char **buffer, int *size, int needed);
/* makeroom returns -1 if needed was far too large; otherwise returns 0. */

/* It doesn't appear to be documented whether [v]snprintf put a null
 * in if they overrun.  GNU libc does, but I don't want to rely on
 * that.
 * So here are some functions that always do, regardless - including
 * versions of strcat and strcpy.  The ...nyt...cat functions take the
 * maximum length of the resulting buffer as size parameter, rather
 * than the maximum length of the added portion.
 *
 * So,
 *   ...n...       specify copied length (inc. any null), may or may not null-terminate
 *   ...ny...      specify copied length (inc. null) and always null-terminate
 *   ...nyt...cat  specify total buffer length and always null-terminate
 */

/* Function names best pronounced with a Russian accent. */
void vsnyprintf(char *buffer, size_t size, const char *fmt, va_list al);
void snyprintf(char *buffer, size_t size, const char *fmt, ...) PRINTFFORMAT(3,4);
void strnycpy(char *dest, const char *src, size_t size);

void vsnytprintfcat(char *buffer, size_t size, const char *fmt, va_list al);
void snytprintfcat(char *buffer, size_t size, const char *fmt, ...) PRINTFFORMAT(3,4);
void strnytcat(char *dest, const char *src, size_t size);

#ifndef HAVE_SETENV
int setenv(const char *name, const char *value, int overwrite);
#endif /* HAVE_SETENV */

#endif /* LIB_H */
