dnl  userv - tokens.h.m4
dnl  token values, passed through m4 with defs from langauge.i4
/*  
 * userv is copyright Ian Jackson and other contributors.
 * See README for full authorship information.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with userv; if not, see <http://www.gnu.org/licenses/>.
 */

include(language.i4)

#ifndef TOKENS_H
#define TOKENS_H

enum tokens {
  tokm_instance=           0x000000ff,
  tokm_repres=             0x00000f00,
  tokm_type=               0xfffff000,
  tokr_nonstring=          0x00000100,
  tokr_word=               0x00000200,
  tokr_punct=              0x00000300,
  tokr_string=             0x00000400,
undivert(4)
undivert(1)
undivert(2)
};

#endif

divert(-1)
undivert
