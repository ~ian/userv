.\"
.\" This manpage is copyright, like the rest of userv, - see the
.\" copyright section, below.
.Dd November 3, 1999
.Dt USERV 1
.Os "userv"
.Sh NAME
.Nm userv
.Nd request user services
.Sh SYNOPSIS
.Nm userv
.Op Ar option ...
.Op Fl -
.Ar service-user
.Ar service-name
.Op Ar argument ...
.Nm userv
.Op Ar option ...
.Fl B | -builtin
.Op Fl -
.Ar builtin-service
.Bk -words
.Op Ar info-argument ...
.Ek
.Sh DESCRIPTION
.Nm userv
is used to have a task performed under different userid while
maintaining limited trust between caller and callee.
.Pp
.Ar service-user
specifies which user account is to perform the task.  The user may be
a login name or a numeric uid, or
.Ql -
to indicate that the service user is to be the same as the calling
user.
.Pp
The service name is interpreted by the userv daemon on behalf of the
service user.  This is controlled by configuration files in the
service user's filespace; consult the userv specification for details.
.Sh OPTIONS
Single-letter options may be combined as is usual with Unix programs,
and the value for such an option may appear in the same argument or in
the next.
.Bl -tag -width Fl
.It Fl B | -builtin
Requests that a builtin service be provided.  This is equivalent to
using the
.Fl -override
option to specify a string consisting of
.Ql execute-builtin
followed by the
.Ar builtin-service
requested, and requesting a service user of 
.Ql -
(indicating the calling user).
.Pp
If the builtin service being requested requires a
.Ar service-argument
then this must be supplied to the client in the
same argument as the
.Ar builtin-service .
See the specification, or the output of
.Bd -literal -offset indent -compact
userv -B help
.Ed
for details of the builtin services available,
and below for details of the
.Fl -override
options.
.Pp
The actual service name passed will be the
.Ar builtin-service ;
note
that this actual service name (as opposed to the override data) and
the
.Ar info-argument Ns s
supplied will be ignored by most builtin
services; the override mechanism and
.Ql execute-builtin
will be
used to ensure that the right builtin service is called with the right
.Ar service-argument Ns s .
.It Xo
.Fl f | -file
.Sm off
.Ar fd Oo Ar fdmodifiers Oc = Ar filename
.Sm on
.Xc
Requests that data be copied in and out of the service using pipes.
For each file or descriptor this will be done by creating a pipe, one
end of which is passed to the service program and the other end of
which is passed to a copy of
.Nm cat
invoked by the client; the other file descriptor passed to
.Nm cat
will be one inherited by the client program from the caller or one
opened by the client program on behalf of the caller.
.Pp
The descriptor in the service program that should be connected must be
specified as
.Ar fd ,
either as a decimal number or as one of the
strings
.Ql stdin ,
.Ql stdout
or
.Ql stderr .
The next argument is a filename which will be opened by the client
with the privileges of the calling user.
.Pp
.Ar modifiers
is used to specify whether the file or descriptor is to be read from
or written to.  It consists of a series of words separated by commas.
A comma may separate the
.Ar modifiers
from
the
.Ar fd
and is required if
.Ar fd
is not numeric.
The modifier words are:
.Bl -tag -width Li
.It Ic read
.Dv O_RDONLY :
Allow reading and not writing.  May not be used with
.Ql write
or things that imply it.
.It Ic write
.Dv O_WRONLY :
Allow writing and not reading.
.Em Doesn't truncate or create
without
.Ql truncate
or
.Ql create .
.Ql write
or things that imply it may not be used with
.Ql read .
.It Ic overwrite
Equivalent to
.Ql write,create,truncate .
.It Ic create ,  creat
.Dv O_CREAT :
Creates the file if necessary.  Implies
.Ql write .
.It Ic exclusive , excl
.Dv O_EXCL:
Fails if the file already exists. Implies write and create. May
not be used with
.Ql truncate .
.It Ic truncate , trunc
.Dv O_TRUNC:
Truncate any existing file.  Implies
.Ql write .
May not be used with
.Ql exclusive .
.It Ic append
.Dv O_APPEND :
All writes will append to the file.  Implies
.Ql write
(but not
.Ql create ) .
.It Ic sync
.Dv O_SYNC :
Do writes synchronously.  Implies
.Ql write .
.It Ic wait , nowait , close
These modifiers control the behaviour of the client, with respect to
the pipes carrying data to and from the service, when the service
terminates.  See below.
.It Ic fd
The
.Ar filename
is not a filename but a numeric file descriptor.
One or both of
.Ql read
and
.Ql write
must be specified, and no
other words are allowed.  The
.Ar filename
may also be
.Ql stdin ,
.Ql stdout
or
.Ql stderr
for file descriptor 0, 1 or 2 respectively.
.El
.Pp
If no
.Ar modifiers
which imply
.Ql read
or
.Ql write
are used it is as if
.Ql write
had been specified, except that if the filedescriptor 0 of the service
is being opened (either specified numerically or with
.Ql stdin )
it is as if
.Ql overwrite
had been specified (or
.Ql write
if only
.Ql fd
was specified).
.Pp
The client will also use
.Dv O_NOCTTY
when opening files specified by the caller, to avoid changing its
controlling terminal.
.Pp
By default
.Va stdin ,
.Va stdout
and
.Va stderr
of the service will be connected to the corresponding descriptors on
the client.  Diagnostics from the client and daemon will also appear
on
.Va stderr .
.Pp
If
.Ql wait
is specified, the client will wait for the pipe to be closed, and only
exit after this has happened.  This means that either the receiving
end of the pipe connection was closed while data was still available
at the sending end, or that the end of file was reached on the reading
file descriptor.  Errors encountered reading or writing in the client
at this stage will be considered a system error and cause the client
to exit with status 255, but will not cause disconnection at the
service side since the service has already exited.
.Pp
If
.Ql close
is specified the client will immediately close the pipe connection by
killing the relevant copy of
.Nm cat .
If the service uses the descriptor it will get
.Dv SIGPIPE
(or
.Er EPIPE )
for a writing descriptor or end of file for a reading one; the
descriptor opened by or passed to the client will also be closed.
.Pp
If
.Ql nowait
is specified then the client will not wait and the
connection will remain open after the client terminates.  Data may
continue to be passed between the inheritors of the relevant
descriptor on the service side and the corresponding file or
descriptor on the client side until either side closes their
descriptor.  This should not usually be specified for
.Va stderr
(or
.Va stdout
if
.Ql "--signals stdout"
is used) since diagnostics from the service side may arrive after the
client has exited and be confused with expected output.
.Pp
The default is
.Ql wait
for writing file descriptors and
.Ql close
for reading ones.
.It Xo
.Fl w | -fdwait
.Ar fd Ns = Ns Ar action
.Xc
Sets the action on termination of the service for the specified file
descriptor;
.Ar action
must be
.Ql wait ,
.Ql nowait
or
.Ql close
as described above.  The file descriptor must be specified as open
when this option is encountered; this option is overridden by any
later
.Fl -file
or
.Fl -fdwait
option - even by a
.Fl -file
which does not specify an action on termination (in this case the
default will be used, as described above).
.It Xo
.Fl D | -defvar
.Ar name Ns = Ns Ar value
.Xc
Set a user-defined variable
.Ar name
to
.Ar value .
These
user-defined variables are made available in the configuration
language as the parameters
.Ql u- Ns Ar name
and are passed to the
service in environment variables
.Ev USERV_U_ Ns Ar name .
.Ar name
may contain only alphanumerics and underscores, and must start with a
letter.  If several definitions are given for the same
.Ar name
then only the last is effective.
.It Fl t | -timeout Ar seconds
Time out the service if it takes longer than
.Ar seconds
seconds (a positive integer, in decimal).  Timeout will produce a
diagnostic on stderr and an exit status of 255.  If
.Ar seconds
is zero then no timeout will be implemented (this is the default).
.It Fl S | -signals Ar method
Affects the handling of the exit status when the service terminates
due to a signal.  (The client will always finish by calling
.Fn _exit ,
so that only numbers from 0 to 255 can be returned and not the full
range of numbers and signal indications which can be returned by the
.Fn wait
family of system calls.)
.Pp
The
.Ar method
may be one of the following:
.Bl -tag -width Li
.It Ar status
The client's exit status will be
.Ar status .
This will not be distinguishable from the service really having exited
with code
.Ar status .
This method is the default, with a
.Ar status
of 254.
.It Ic number , number-nocore
The client's exit status will be the number of the signal which caused
the termination of the service.  If
.Ql number
is used rather than
.Ql number-nocore
then 128 will be added if the service dumped core.
.Ql number
is very like the exit code mangling done by the Bourne shell.
.It Ic highbit
The client's exit status will be the number of the signal with 128
added.  If the service exits normally with an exit code of greater
than 127 then 127 will be returned.
.It Ic stdout
The service's numeric wait status as two decimal numbers (high byte
first) and a textual description of its meaning will be printed to the
client's standard output.  It will be preceded by a newline and
followed by an extra newline, and the numbers are separated from each
other and from the textual description by single spaces.  The exit
status of the client will be zero, unless a system error occurs in
which case no exit status and description will be printed to
.Va stdout ,
and an error message will be printed to
.Va stderr
as usual.
.Pp
Problems such as client usage errors, the service not being found or
permission being denied or failure of a system call are system errors.
An error message describing the problem will be printed on the
client's
.Va stderr ,
and the client's exit status will be 255.  If the client dies due to a
signal this should be treated as a serious system error.
.El
.It Fl H | -hidecwd
Prevents the calling process's current directory name from being
passed to the service; the null string will be passed instead.
.It Fl P | -sigpipe
If the service program is terminated due to a
.Dv SIGPIPE
the exit status of the client will be zero, even if it would have been
something else according to the exit status method specified.  This
option has no effect on the code and description printed if the exit
status method
.Ql stdout
is in use.
.It Fl h | -help
Prints the client's usage message.
.It Fl -copyright
Prints the copyright and lack of warranty notice.
.El
.Sh SECURITY-OVERRIDING OPTIONS
There are also some options which are available for debugging and to
allow the system administrator to override a user's policy.  These
options are available only if the client is called by root or if the
calling user is the same as the service user.
.Bl -tag -width Fl
.It Fl -override Ar configuration-data
.It Fl -override-file Ar file
Do not read the usual configuration files.  Instead, the client sends
.Ar configuration-data
(followed by a newline) or the contents of
.Ar filename
(which is opened in the context of the client) to the daemon and the
daemon uses that data instead.  The
.Ar configuration-data
must all be in one argument.  It will have a single newline appended
so that a single directive can easily be given, but if more than one
directive is required it will have to contain one or more real
newlines.
.It Fl -spoof-user Ar user
Pretend to the service that it is being called by
.Ar user
(which may be a username or a uid).  This will also affect the group
and supplementary groups supplied to the service; they will be the
standard group and supplementary groups for
.Ar user .
The
.Fl -spoof-user
option will
.Em not
affect which user is chosen if the service user is specified as just
.Ql - ;
in this case the service user will be the real calling user.
.El
.Sh ENVIRONMENT
.Bl -tag -width Ev
.It Ev LOGNAME , USER
These are used to determine the name of the calling user, to be passed
to the service in
.Ev USERV_USER .
Their values will only be used if they correspond to the calling UID.
.El
.Sh FILES
.Bl -tag -width Pa
.It Pa /var/run/userv/socket
.Ux Ns -domain
socket used for communication between
.Nm
and
.Nm uservd .
.It Pa /var/run/userv/%x.%x.%x
Pipes used for connecting file descriptors in the client and the
service.
.El
.Sh SEE ALSO
.Xr uservd 8
.Rs
.%T "User service daemon and client specification"
.%A Ian Jackson
.Re
.Sh COPYRIGHT
GNU userv is copyright Ian Jackson and other contributors.
See README or
.Dv userv --copright
for full authorship information.
.Pp
GNU userv is licensed under the terms of the GNU General Public
Licence, version 3 or (at your option) any later version, and it comes
with NO WARRANTY, not even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for details.
.Pp
You should have received a copy of the GNU General Public License
along with userv, if not, see http://www.gnu.org/licenses/
.Sh HISTORY
.Nm
was initially written in 1996 by Ian Jackson.  It became
.Tn GNU
.Nm
in 1999, and version 1.0 was released in 2000.
